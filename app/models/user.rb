class User < ActiveRecord::Base

	has_many :microposts
	has_many :postingans, :foreign_key => "actor_id"
end
