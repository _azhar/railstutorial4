class PostingansController < ApplicationController
  before_action :set_postingan, only: [:show, :edit, :update, :destroy]

  # GET /postingans
  # GET /postingans.json
  def index
    @postingans = Postingan.all
  end

  # GET /postingans/1
  # GET /postingans/1.json
  def show
  end

  # GET /postingans/new
  def new
    @postingan = Postingan.new
  end

  # GET /postingans/1/edit
  def edit
  end

  # POST /postingans
  # POST /postingans.json
  def create
    @postingan = Postingan.new(postingan_params)

    respond_to do |format|
      if @postingan.save
        format.html { redirect_to @postingan, notice: 'Postingan was successfully created.' }
        format.json { render action: 'show', status: :created, location: @postingan }
      else
        format.html { render action: 'new' }
        format.json { render json: @postingan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /postingans/1
  # PATCH/PUT /postingans/1.json
  def update
    respond_to do |format|
      if @postingan.update(postingan_params)
        format.html { redirect_to @postingan, notice: 'Postingan was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @postingan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /postingans/1
  # DELETE /postingans/1.json
  def destroy
    @postingan.destroy
    respond_to do |format|
      format.html { redirect_to postingans_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_postingan
      @postingan = Postingan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def postingan_params
      params.require(:postingan).permit(:content, :actor_id)
    end
end
