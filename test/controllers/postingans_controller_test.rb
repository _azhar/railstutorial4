require 'test_helper'

class PostingansControllerTest < ActionController::TestCase
  setup do
    @postingan = postingans(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:postingans)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create postingan" do
    assert_difference('Postingan.count') do
      post :create, postingan: { actor_id: @postingan.actor_id, content: @postingan.content }
    end

    assert_redirected_to postingan_path(assigns(:postingan))
  end

  test "should show postingan" do
    get :show, id: @postingan
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @postingan
    assert_response :success
  end

  test "should update postingan" do
    patch :update, id: @postingan, postingan: { actor_id: @postingan.actor_id, content: @postingan.content }
    assert_redirected_to postingan_path(assigns(:postingan))
  end

  test "should destroy postingan" do
    assert_difference('Postingan.count', -1) do
      delete :destroy, id: @postingan
    end

    assert_redirected_to postingans_path
  end
end
