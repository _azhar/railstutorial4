class CreatePostingans < ActiveRecord::Migration
  def change
    create_table :postingans do |t|
      t.string :content
      t.integer :actor_id

      t.timestamps
    end
  end
end
